﻿using System;
using HSP_Vorlage_Trigonometrie;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Trigonometrie.UnitTests
{
    [TestClass]
    public class TrigonometrieUnitTests
    {
        [TestMethod]
        public void TrigonometrieTestLaengen()
        {
            Program prg = new Program();

            var Laengen = prg.Laengen(1, 5, 8, 10, 7, 16);

            CollectionAssert.Contains(Laengen, 8.602);
            CollectionAssert.Contains(Laengen, 12.53);
            CollectionAssert.Contains(Laengen, 6.083);
        }

        [TestMethod]
        public void TrigonometrieTestWinkel()
        {
            Program prg = new Program();

            var Winkel = prg.Winkel(7.777, 5.555, 8.888);

            CollectionAssert.Contains(Winkel, 60.000);
            CollectionAssert.Contains(Winkel, 38.213);
            CollectionAssert.Contains(Winkel, 81.787);
        }

        [TestMethod]
        public void TrigonometrieTestUmfangFlaeche()
        {
            Program prg = new Program();

            var UmfangFlaeche = prg.UmfangFlaeche(3.333,5.555,7.777);

            CollectionAssert.Contains(UmfangFlaeche, 3.333+5.555+7.777);
            CollectionAssert.Contains(UmfangFlaeche, 8.017);
        }
    }
}

# Trigonometrie

Aufgabe Trigonometrie für Hochsprachenprogrammierung / Mecke an der Jade Hochschule

Aufgabe: Füge den Code hinzu, der die Seitenlängen, Winkel, Fläche und den Umfang eines beliebigen Dreiecks berechnet.


Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers

Diesen Ablauf können Sie auch mit Hilfe des Videos "Aufgabensammlung Einstieg" nachvollziehen

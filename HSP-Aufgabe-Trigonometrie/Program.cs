﻿using System;

namespace HSP_Aufgabe_Trigonometrie
{
    public class Program
    {
        public double[] Laengen(double Ax, double Ay, double Bx, double By, double Cx, double Cy)
        {
            //  Variablendeklaration
            double a;
            double b;
            double c;

            //  Berechne hier die 3 Seitenlängen aus den eingegebenen Kooridnaten. Wie in der Vektorrechnung im ersten Semester. (Wurzel aus (Ax-Bx)²+(Ay-By)²)

            // <hier fehlt Ihr Code>



            //  Runden zur Überprüfung und um relevante Daten Auszugeben
            a = Math.Round(a, 3);
            b = Math.Round(b, 3);
            c = Math.Round(c, 3);

            //  Rückgabe an die Main Methode
            return new double[3] { a, b, c }; // Rückgabe der Werte des Ergebnis als Arrays
        }

        public double[] Winkel(double a, double b, double c)
        {
            //  Variablendeklaration
            double winkelA;
            double winkelB;
            double winkelC;

            //  Berechne hier die 3 Winkel mit dem Kosinussatz.

            // <hier fehlt Ihr Code>



            //  Runden zur Überprüfung und um relevante Daten Auszugeben
            winkelA = Math.Round(winkelA, 3);
            winkelB = Math.Round(winkelB, 3);
            winkelC = Math.Round(winkelC, 3);

            //  Rückgabe an die Main Methode
            return new double[3] { winkelA, winkelB, winkelC }; // Rückgabe der Werte des Ergebnis als Arrays
        }

        public double[] UmfangFlaeche(double a, double b, double c)
        {
            //  Variablendeklaration
            double umfang;
            double flaeche;

            //  Berechne hier die Fläche sowie den Umfang des Dreiecks.

            // <hier fehlt Ihr Code>




            //  Runden zur Überprüfung und um relevante Daten Auszugeben
            flaeche = Math.Round(flaeche, 3);

            //  Rückgabe an die Main Methode
            return new double[2] { umfang, flaeche }; // Rückgabe der Werte des Ergebnis als Arrays
        }


        static void Main(string[] args)
        {
            // Erzeugt ein Objekt vom Typ Program, weches wir dann über prg ansprechen können
            Program prg = new Program(); 

            // Deklaration der Variablen
            double Ax, Ay, Bx, By, Cx, Cy;

            //  Eingabe
            Console.WriteLine("\nGebe nacheinander die Kooardinaten der Eckpunkte A,B und C an. ");

            Console.Write("\nA x: ");
            double.TryParse(Console.ReadLine(), out Ax);    // TryParse versucht eine Transformation eines Strings in den gegebenen Typ (double)
                                                            // Wenn dies erfolgreich ist, dann ist das Ergebnis "true" (Boolean) und das Ergebnis
                                                            // wird zusaätzlich via einem out Parameter zurückgegeben
            Console.Write("A y: ");
            double.TryParse(Console.ReadLine(), out Ay);

            Console.Write("\nB x: ");
            double.TryParse(Console.ReadLine(), out Bx);
            Console.Write("B y: ");
            double.TryParse(Console.ReadLine(), out By);

            Console.Write("\nC x: ");
            double.TryParse(Console.ReadLine(), out Cx);
            Console.Write("C y: ");
            double.TryParse(Console.ReadLine(), out Cy);

            //  Methodenaufruf
            double[] Laengen = prg.Laengen(Ax, Ay, Bx, By, Cx, Cy);
            double[] Winkel = prg.Winkel(Laengen[0], Laengen[1], Laengen[2]);
            double[] UmfangFlaeche = prg.UmfangFlaeche(Laengen[0], Laengen[1], Laengen[2]);

            //  Ausgabe
            Console.WriteLine("\n-------------------\nLängen: \na= " + Laengen[0] + " cm \nb= " + Laengen[1] + " cm \nc= " + Laengen[2] + " cm\n-------------------\n");
            Console.WriteLine("\n-------------------\nWinkel: \nAplpha= " + Winkel[0] + " Grad \nBeta= " + Winkel[1] + " Grad \nGamma= " + Winkel[2] + " Grad\n-------------------\n");
            Console.WriteLine("\n-------------------\nUmfang=  " + UmfangFlaeche[0] + " cm\nFläche= " + UmfangFlaeche[1] + " cm*2\n-------------------");

            //  Press any key to close
            Console.WriteLine("\nDrücke eine beliebige Taste zum Beenden.");
            Console.ReadKey();
        }
    }
}
